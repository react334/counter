import React, { useState } from 'react';
import Button from '../Button';

const FunctionalCounter = () => {
  const [count, setCount] = useState(1);

  const increment = () => {
    setCount((currentCount) => (currentCount + 1));
  }

  const decrement = () => {
    setCount((currentCount) => (currentCount - 1));
  }

  return (
    <div className="counter">
      <span>
        Counter:
      </span>
      <Button value="+" onClick={increment} />
      {count}
      <Button value="-" onClick={decrement} />
    </div>
  );
};

export default FunctionalCounter;
