import React from 'react';
import './Counter.css';
import ClassCounter from '../ClassCounter';
import FunctionalCounter from '../FunctionalCounter';

const Counter = () => {
  return (
    <div className="counter-wrapper">
      <ClassCounter />
      <FunctionalCounter />
    </div>
  );
}

export default Counter;
