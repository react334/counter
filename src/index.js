import React from 'react';
import ReactDOM from 'react-dom/client';

// import style
import './style.css';

// import component
import Counter from './Counter/Counter';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Counter />
  </React.StrictMode>
);
