import React from 'react';
import Button from '../Button';

class ClassCounter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      count: 1
    }
  }

  increment() {
    this.setState((prevState) => ({ count: prevState.count + 1 }));
    this.setState({ count: this.state.count + 1 });
  }

  decrement() {
    this.setState((prevState) => ({ count: prevState.count - 1 }));
  }

  render() {
    return (
      <div className="counter">
        <span>Counter:
        </span>
        <Button value="+" onClick={this.increment.bind(this)} />
        {this.state.count}
        <Button value="-" onClick={this.decrement.bind(this)} />
      </div>
    );
  }
}

export default ClassCounter;
